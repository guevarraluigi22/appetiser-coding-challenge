Persistence - I used UserDefaults to set data and get data, I usually use this for small data fetching.
                        Here on the App, when you kill the app it will save the last time you are active.

Architecture - I used MVVM for my code design pattern so I won't deal with lots of view controllers, and I feel like this is                        more organized, also on top of that I developed this app using SwiftUI, I find it very efficient and time   consuming when using SwiftUI than using traditional StoryBoards that lags when you already have lots of views and controls, though for me it is more of personal preference as a developer. 



Hope To Work With You Soon!

- Luigi P. Guevarra
