//
//  RowView.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import SwiftUI

//View of each row from the list
struct RowView:View {
    
    //item result from the model
    var item : TrackModelResult
    
    var body: some View {
        HStack{
            MyTrackImageView(url: item.artworkUrl100 ?? "", height: 100, width: 100)
            
            VStack(alignment: .leading){
                Text("\(item.trackName ?? "N/A")").bold()
                Text("\(item.primaryGenreName ?? "N/A")").italic()
                Text("$ \((item.trackPrice ?? 0.00), specifier: "%.2f")")
            }
            
        }
    }
    
}
