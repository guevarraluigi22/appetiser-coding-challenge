//
//  DetailView.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import SwiftUI

//View of the Detail when clicked on the list
struct DetailView: View {
    
    //item result of the model
    var item :TrackModelResult
    
    var body: some View {
        VStack(alignment: .leading) {
            MyTrackImageView(url: item.artworkUrl100 ?? "", height: 200, width: UIScreen.main.bounds.width - 40)
            if item.longDescription != nil {
                Text("\(item.longDescription ?? "Description not available.")")
            } else {
                Text("Description not available.")
            }
            Spacer()
        }
        .frame(width: UIScreen.main.bounds.width - 40)
        .padding(.top, 20)
        .navigationBarTitle(Text("Detail"))
    }
}
