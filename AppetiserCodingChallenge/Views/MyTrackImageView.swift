//
//  MyTrackImageView.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

//Image that will display on the list and detail
struct MyTrackImageView:View {
    
    //url from the API
    var url : String
    
    //height of the image
    var height : CGFloat
    
    //width of the image
    var width : CGFloat
    
    var body : some View {
        WebImage(url: URL(string: self.url))
            .onSuccess { image, cacheType in
                // Success
            }
            .resizable() // Resizable like SwiftUI.Image, you must use this modifier or the view will use the image bitmap size
            .placeholder(Image(systemName: "photo")) // Placeholder Image
            // Supports ViewBuilder as well
            .placeholder {
                Rectangle().foregroundColor(.gray)
            }
            .indicator(.activity) // Activity Indicator
            .animation(.easeInOut(duration: 0.5)) // Animation Duration
            .transition(.fade) // Fade Transition
            .scaledToFit()
            .frame(width: self.width, height: self.height, alignment: .center)
            .background(Color.black)
        .cornerRadius(10)
    }
    
}
