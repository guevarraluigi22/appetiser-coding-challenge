//
//  MainView.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import SwiftUI

//Main View of the application, contains the list
struct MainView: View {
    
    //viewmodel of the tracks
    @ObservedObject var trackViewModel = TrackViewModel()
    
    var body: some View {
        
        NavigationView {
            VStack(alignment : .leading){
                
                if(MyDefaults.getString("userLastLogin") == nil || MyDefaults.getString("userLastLogin") == ""){
                    Text("First Visit! Enjoy!").padding(.leading, 20)
                }else{
                    Text("User last visit : \(MyDefaults.getString("userLastLogin"))").padding(.leading, 20)
                }
                
                List{
                    ForEach(self.trackViewModel.trackModelResult, id: \.trackId){ item in
                        
                        NavigationLink(
                            destination: DetailView(item: item)
                        ){
                            RowView(item: item)
                        }
                        
                    }
                }

            }.onAppear(){
                self.trackViewModel.getResult(){ response in
                    print(response)
                }
            }
        .navigationBarTitle("Tracks")
        }.navigationViewStyle(DoubleColumnNavigationViewStyle())
    }
    
}

//date format that will show on the header
private let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .medium
    dateFormatter.timeStyle = .medium
    return dateFormatter
}()

//Preview of Main View, can be seen of the canvass editor
struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
