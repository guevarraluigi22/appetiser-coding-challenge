//
//  formatDate.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation

class formatDate{
    
    class func StringToDate(str: String, format:String)->Date{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: str)!
    }
    
    class func DateToString(date: Date, format:String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    
    
}
