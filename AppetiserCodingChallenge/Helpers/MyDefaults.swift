//
//  MyDefaults.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation

class MyDefaults {
    
    class func saveString(_ key: String!, value: String!) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    class func removeString(_ key: String!) {
        let defaults = UserDefaults.standard
        //        defaults.setObject(nil, forKey: key)
        defaults.removeObject(forKey: key)
    }
    
    class func getString(_ key: String!) -> String! {
        let defaults = UserDefaults.standard
        var str = defaults.string(forKey: key)
        
        if(str == nil){
            str = ""
        }
        
        return str
    }
     
}
