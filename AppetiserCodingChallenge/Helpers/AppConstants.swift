//
//  AppConstants.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import Foundation
import SwiftUI

//url of the api
let apiUrl = "https://itunes.apple.com/search?"

//api header
let apiHeaderFields = ["Accept":"application/json",
                       "Content-Type":"application/json"
                      ]
//parameters
let apiParams = ["term":"star",
                 "amp;country":"au",
                 "amp;media":"movie",
                 "amp;all":""
                ]
