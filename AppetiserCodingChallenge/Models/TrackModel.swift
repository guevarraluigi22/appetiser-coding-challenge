//
//  TrackModel.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import SwiftUI

struct TrackModel : Decodable {
    
    var resultCount : Int
    var results : [TrackModelResult]
    
}

struct TrackModelResult : Decodable {
    
    var wrapperType : String?
    var kind : String?
    var artistId : Int?
    var collectionId : Int?
    var trackId : Int?
    var artistName : String?
    var collectionName : String?
    var trackName : String?
    var collectionCensoredName : String?
    var trackCensoredName : String?
    var artistViewUrl : String?
    var collectionViewUrl : String?
    var trackViewUrl : String?
    var previewUrl : String?
    var artworkUrl30 : String?
    var artworkUrl60 : String?
    var artworkUrl100 : String?
    var collectionPrice : Double?
    var trackPrice : Double?
    var releaseDate : String?
    var collectionExplicitness : String?
    var trackExplicitness : String?
    var discCount : Int?
    var discNumber : Int?
    var trackCount : Int?
    var trackNumber : Int?
    var trackTimeMillis : Double?
    var country : String?
    var currency : String?
    var primaryGenreName : String?
    var isStreamable : Bool?
    var contentAdvisoryRating : String?
    var shortDescription : String?
    var longDescription : String?
    
}
