//
//  TrackViewModel.swift
//  AppetiserCodingChallenge
//
//  Created by Luigi Guevarra on 3/20/20.
//  Copyright © 2020 Luigi Guevarra. All rights reserved.
//

import SwiftUI

class TrackViewModel: ObservableObject {
    
    //array of trackmodelresult which will hold the result data that will reflect on the list
    @Published var trackModelResult: [TrackModelResult] = []
    @Published var loading = true

    func getResult(completionHandler: @escaping ( _ response:[String:String]) -> Void){
                
            guard let urlComp = NSURLComponents(string: apiUrl) else { return }
            
            var items = [URLQueryItem]()
            
            for (key,value) in apiParams {
                items.append(URLQueryItem(name: key, value: value))
            }
            
            items = items.filter{!$0.name.isEmpty}
            
            if !items.isEmpty {
              urlComp.queryItems = items
            }
            
            var request = URLRequest(url: urlComp.url!)
            
            request.httpMethod = "GET"

            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
    
                if let error = error {
                    print(error.localizedDescription)
                    completionHandler(["success":"false","message":"\(error.localizedDescription)"])
                    return
                }
    
                guard let response = response as? HTTPURLResponse else { return }
                if response.statusCode == 200 {
                    guard let data = data else { return }
                    DispatchQueue.main.async {
                        do {
                            let result = try JSONDecoder().decode(TrackModel.self, from: data)
                            self.trackModelResult = result.results
                            //print(result)
                            if(self.trackModelResult.count > 0){
                                completionHandler(["success":"true","message":""])
                            }else{
                                completionHandler(["success":"false","message":"No Tracks found."])
                            }
                        
                            self.loading = false
                        } catch let err {
                            print("Error: \(err)")
                            completionHandler(["success":"false","message":"\(err.localizedDescription)"])
                            self.loading = false
                        }
                    }
                } else {
                    print("HTTPURLResponse code: \(response.statusCode)")
                    completionHandler(["success":"false","message":"HTTPURLResponse code: \(response.statusCode)"])
                    self.loading = false
                }
            }.resume()
        
    }
    
}

